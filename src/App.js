import { BrowserRouter } from "react-router-dom";
import { routerConfig } from "./router";
import { Router } from "./components";

function App() {
  return (
    <BrowserRouter>
      <Router routes={routerConfig} />
    </BrowserRouter>
  );
}

export default App;
