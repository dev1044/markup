import { lazy } from "react";

import Arbeitnehmer from "../modules/Jobs/Arbeitnehmer";
import Arbeitgeber from "../modules/Jobs/Arbeitgeber";
import Temporärbüro from "../modules/Jobs/Temporärbüro";

const routes = [
  // Router configuration - list of available routes
  {
    path: "/",
    exact: true,
    private: false,
    component: lazy(() => import("../modules/Home/Home.component")),
    fallback: <div> Loading... </div>,
  },
  {
    path: "/jobs",
    private: false,
    component: lazy(() => import("../modules/Jobs/Jobs.component")),
    fallback: <div> Loading... </div>,
    routes: [
      {
        path: "/jobs/arbeitnehmer",
        private: false,
        component: Arbeitnehmer,
        fallback: <div> Loading... </div>,
      },
      {
        path: "/jobs/arbeitgeber",
        private: false,
        component: Arbeitgeber,
        fallback: <div> Loading... </div>,
      },
      {
        path: "/jobs/temporärbüro",
        private: false,
        component: Temporärbüro,
        fallback: <div> Loading... </div>,
      },
    ],
  },
];

export default routes;
