import "./FreeRegistration.styles.css";

function FreeRegistration() {
  return (
    <div className="flex w-full h-[90px] bottom-0 bg-white bg-no-repeat shadow-free-reg rounded-tl-xl rounded-tr-xl z-10 fixed max-w-[640px]">
      <button className="reg-button h-[40px] w-[320px] ml-auto mr-auto mt-[24px] text-[14px] text-clear-day-500 justify-center rounded-xl">
        Kostenlos Registrieren
      </button>
    </div>
  );
}

export default FreeRegistration;
