import "./JobsMainBlock.styles.css";
import ShakeHands from "../../assets/images/undraw_agreement_aajr.svg";

function JobsMainBlock() {
  return (
    <div className="w-full mt-[15px] jobs-main-container relative flex justify-center flex-col">
      <div className="jobs-text-block text-center text-[42px]">
        <p className=" text-oxford-blue-500">
          Deine Job <br />
          website
        </p>
      </div>
      <div className="flex flex-col items-center">
        <img className="max-w-[360px]" alt="shakehands" src={ShakeHands} />
      </div>
    </div>
  );
}

export default JobsMainBlock;
