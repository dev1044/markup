import "./SlideContentFirstBlock.styles.css";

//Parameterized component of SliderContent with optional text and image
function SlideContentFirstBlock({ mainText, BGImage }) {
  return (
    <div className="relative flex items-end w-[320px] h-[265px] mt-[10px]">
      <div className="text-slate-gray-500 flex z-10">
        <p className="text-[130px]">1.</p>
        <p className="text-[16px] self-end mb-[44px] ml-[32px]">{mainText}</p>
      </div>
      <img
        className="absolute top-0 left-[31%] w-[220px] h-[145px]"
        alt="bgimage"
        src={BGImage}
      />
      <div className="gray-dot"></div>
    </div>
  );
}

export default SlideContentFirstBlock;
