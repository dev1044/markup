import { Suspense } from "react";
import { Redirect, Route } from "react-router-dom";

export function RouteWithSubRoutes({
  authenticated,
  fallback,
  path,
  redirect,
  isRequiredFields,
  ...route
}) {
  return (
    <Suspense fallback={fallback}>
      <Route
        path={path}
        render={(props) => {
          if (redirect) {
            return <Redirect to={redirect} />;
          }
          return (
            route.component && (
              <route.component {...props} routes={route.routes} />
            )
          );
        }}
      />
    </Suspense>
  );
}

export default RouteWithSubRoutes;
