import { Switch } from "react-router";

import RouteWithSubRoutes from "./RouteWithSubRoutes.component";

export function Router({ routes }) {
  return (
    <Switch>
      {routes.map((route) => (
        <RouteWithSubRoutes key={route.path} {...route} />
      ))}
    </Switch>
  );
}
export default Router;
