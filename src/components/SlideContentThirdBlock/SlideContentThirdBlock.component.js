import "./SlideContentThirdBlock.styles.css";
//Parameterized component of SliderContent with optional text and image, pageNumber is used to set additional styling
function SlideContentThirdBlock({ mainText, BGImage, pageNumber }) {
  return (
    <div className="relative flex items-end ml-[54px] w-[299px] h-[327px] ">
      <div className="text-slate-gray-500 flex self-start z-10 leading-none">
        <p className="text-[130px]">3.</p>
        <p className="text-[16px] self-end mb-[14px] ml-[32px]">{mainText}</p>
      </div>
      <img
        className={`absolute  left-0 w-[281px] h-[210px] z-10 ${
          pageNumber === 2 ? "mt-[30px]" : pageNumber === 3 ? "mt-[30px]" : ""
        }`}
        alt="bgimage"
        src={BGImage}
      />
      <div
        className={`${
          pageNumber === 2
            ? "gray-dot-third-dark"
            : pageNumber === 3
            ? "gray-dot-third-dark"
            : "gray-dot-third"
        }`}
      ></div>
    </div>
  );
}

export default SlideContentThirdBlock;
