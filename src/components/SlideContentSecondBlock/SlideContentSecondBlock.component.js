import "./SlideContentSecondBlock.styles.css";
//Parameterized component of SliderContent with optional text and image, pageNumber is used to set additional styling
function SlideContentSecondBlock({ mainText, BGImage, pageNumber }) {
  return (
    <div className="w-full slide-content-bg">
      <div className=" relative flex items-end ml-[35px] w-[310px] h-[281px] ">
        <div className="text-slate-gray-500 flex self-start z-10 leading-none">
          <p className="text-[130px]">2.</p>
          <p className="text-[16px] self-end mb-[14px] ml-[32px]">{mainText}</p>
        </div>
        <img
          className={`absolute  ${
            pageNumber === 2
              ? "w-[258px] h-[179px] left-[5%]"
              : pageNumber === 3
              ? "w-[218px] h-[148px] left-[24%]"
              : "w-[180px] h-[126px] left-[24%]"
          } `}
          alt="bgimage"
          src={BGImage}
        />
      </div>
    </div>
  );
}

export default SlideContentSecondBlock;
