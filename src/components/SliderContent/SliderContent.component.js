function SliderContent({ children, mainText }) {
  return (
    <div className="w-full h-[975px] overflow-hidden bg-clear-day-300">
      <div className="jobs-text-block ml-[10px] mt-[20px] mr-[10px] text-center text-[21px]">
        <p className="w-[280px] text-river-bed-500">{mainText}</p>
      </div>
      {children}
    </div>
  );
}

export default SliderContent;
