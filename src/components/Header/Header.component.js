import "./Header.styles.css";

function Header() {
  return (
    <div className="fixed w-full h-[67px] bg-white bg-no-repeat shadow-header-shadow rounded-l-xl rounded-r-xl z-10 max-w-[640px]">
      <div className="nav-header-wrap"></div>
      <div className="flex flex-row-reverse h-full  text-lochinvar-500 text-[14px]">
        <button className="nav-button bg-none borde-none m-0 p-0 mr-[19px] font-semibold">
          Login
        </button>
      </div>
    </div>
  );
}

export default Header;
