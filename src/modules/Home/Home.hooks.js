import { useCallback } from "react";
import { useHistory } from "react-router-dom";

export function useNavigateToJobs() {
  //Navigation hook - to navigate to jobs page
  const history = useHistory();
  return useCallback(() => {
    history.push(`/jobs/arbeitnehmer`);
  }, [history]);
}
