import { useNavigateToJobs } from "./Home.hooks";
import "./Home.styles.css";

function Home() {
  const navigateToJobs = useNavigateToJobs(); //Navigation callback - navigate to Jobs page
  return (
    <div className="home-container" onClick={navigateToJobs}>
      <div className="red-dot"></div>
      <div className="home-text-block">
        <p>Test</p>
        <p>Mobile</p>
        <p>Index</p>
      </div>
    </div>
  );
}

export default Home;
