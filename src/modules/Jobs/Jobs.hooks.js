import { matchPath, useLocation, useHistory } from "react-router";
import { useCallback, useEffect } from "react";

export function useInitialTab(routes) {
  const location = useLocation();
  return routes.reduce((result, route, key) => {
    if (matchPath(location.pathname, { path: route.path })) {
      result = key;
    } 
    return result;
  }, 0);
}

export function useRedirectToDefaultPage(routes) {
  const history = useHistory();
  const location = useLocation();
  useEffect(() => {
    const path = routes[0].path;
    if (matchPath(location.pathname, { path: "/jobs", exact: true })) {
      history.push(path);
    }
  }, [history, location.pathname, routes]);
}

export function useOnTabPressHandler() {
  const history = useHistory();

  return useCallback(
    (tab) => {
      history.push(tab.path);
    },
    [history]
  );
}
