export const TAB_DATA = [
  { title: "Arbeitnehmer", path: "/jobs/arbeitnehmer" },
  { title: "Arbeitgeber", path: "/jobs/arbeitgeber" },
  { title: "Temporärbüro", path: "/jobs/temporärbüro" },
];
