import {
  SliderContent,
  SlideContentFirstBlock,
  SlideContentSecondBlock,
  SlideContentThirdBlock,
} from "../../../components";
import BGImageFirst from "../../../assets/images/undraw_Profile_data_re_v81r.svg";
import BGImageSecond from "../../../assets/images/undraw_job_offers_kw5d.svg";
import BGImageThird from "../../../assets/images/undraw_business_deal_cpi9.svg";

export default function Temporärbüro() {
  return (
    <SliderContent mainText="Drei einfache Schritte zur Vermittlung neuer Mitarbeiter">
      <SlideContentFirstBlock
        mainText="Erstellen dein Unternehmensprofil"
        BGImage={BGImageFirst}
      />
      <SlideContentSecondBlock
        mainText="Erhalte Vermittlungs- angebot von Arbeitgeber"
        BGImage={BGImageSecond}
        pageNumber={3}
      />
      <SlideContentThirdBlock
        mainText="Vermittlung nach Provision oder Stundenlohn"
        BGImage={BGImageThird}
        pageNumber={3}
      />
    </SliderContent>
  );
}
