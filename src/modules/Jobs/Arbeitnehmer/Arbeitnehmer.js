import {
  SliderContent,
  SlideContentFirstBlock,
  SlideContentSecondBlock,
  SlideContentThirdBlock,
} from "../../../components";
import BGImageFirst from "../../../assets/images/undraw_Profile_data_re_v81r.svg";
import BGImageSecond from "../../../assets/images/undraw_task_31wc.svg";
import BGImageThird from "../../../assets/images/undraw_personal_file_222m.svg";

export default function Arbeitnehmer() {
  return (
    <SliderContent mainText="Drei einfache Schritte zu deinem neuen Job">
      <SlideContentFirstBlock
        mainText="Erstellen dein Lebenslauf"
        BGImage={BGImageFirst}
      />
      <SlideContentSecondBlock
        mainText="Erstellen dein Lebenslauf"
        BGImage={BGImageSecond}
      />
      <SlideContentThirdBlock
        mainText="Mit nur einem Klick bewerben"
        BGImage={BGImageThird}
      />
    </SliderContent>
  );
}
