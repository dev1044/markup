import { DefaultTabBar, Tabs } from "rmc-tabs";

import { Header, JobsMainBlock } from "../../components";
import "./Jobs.styles.css";

import { RouteWithSubRoutes } from "../../components";
import { TAB_DATA } from "./Jobs.constants";
import "rmc-tabs/assets/index.css";
import { FreeRegistration } from "../../components";
import {
  useRedirectToDefaultPage,
  useInitialTab,
  useOnTabPressHandler,
} from "./Jobs.hooks";

function Jobs({ routes }) {
  useRedirectToDefaultPage(routes);
  const k = useInitialTab(routes);
  const onTabPress = useOnTabPressHandler();
  return (
    <div className="flex flex-col relative overflow-auto jobs-module mb-[130px]">
      <Header />
      <div className="jobs-bg mt-[67px]">
        <JobsMainBlock />
      </div>
      <div className="mt-[130px] overflow-x-hidden overflow-y-hidden">
        <Tabs
          tabs={TAB_DATA}
          initialPage={2}
          tabBarActiveTextColor="white"
          tabBarActiveBackgroundColor
          tabBarInactiveTextColor="#319795"
          tabBarUnderlineStyle={{ display: "none" }}
          renderTabBar={(props) => (
            <DefaultTabBar {...props} page={2.5} activeTab={k} />
          )}
          onChange={onTabPress}
        >
          {routes.map((route) => (
            <RouteWithSubRoutes key={route.path} {...route} />
          ))}
        </Tabs>
      </div>

      <FreeRegistration />
    </div>
  );
}

export default Jobs;
