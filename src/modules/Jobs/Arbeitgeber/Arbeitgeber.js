import {
  SliderContent,
  SlideContentFirstBlock,
  SlideContentSecondBlock,
  SlideContentThirdBlock,
} from "../../../components";
import BGImageFirst from "../../../assets/images/undraw_Profile_data_re_v81r.svg";
import BGImageSecond from "../../../assets/images/undraw_about_me_wa29.svg";
import BGImageThird from "../../../assets/images/undraw_swipe_profiles1_i6mr.svg";

export default function Arbeitgeber() {
  return (
    <SliderContent mainText="Drei einfache Schritte zu deinem neuen Mitarbeiter">
      <SlideContentFirstBlock
        mainText="Erstellen dein Unternehmensprofil"
        BGImage={BGImageFirst}
      />
      <SlideContentSecondBlock
        mainText="Erstellen dein Jobinserat"
        BGImage={BGImageSecond}
        pageNumber={2}
      />
      <SlideContentThirdBlock
        mainText="Wähle deinen neuen Mitarbeiter aus"
        BGImage={BGImageThird}
        pageNumber={2}
      />
    </SliderContent>
  );
}
